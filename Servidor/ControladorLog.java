import java.io.*;

public class ControladorLog {

	private PrintWriter log;
	private FileWriter fichero;
	
	public ControladorLog(){
		//abrir fichero y lo dejamos listo para escribir
		fichero = null;
	}
	
	public synchronized void println(String cadena){
		try {
			fichero = new FileWriter("log.txt",true); //el flag true, activa el modo "añadir"
			log = new PrintWriter(fichero);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		log.println(cadena);
		
		try {
			if (null != fichero)
				fichero.close();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	
	}
	
	public void iniciarLog(String cadena){
		try{	
			File fichero = new File("log.txt");
			BufferedWriter bw;
			if(!fichero.exists()) {
				bw = new BufferedWriter(new FileWriter("log.txt"));
				bw.write(cadena);
				bw.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
