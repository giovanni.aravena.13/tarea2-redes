import java.io.*;
import java.util.*;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.ServerSocket;
import java.net.Socket;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import mjson.Json;

public class WorkerThread extends Thread {	

	private static ServerSocket server;

	WorkerThread(ServerSocket serv) {
		server = serv;
	}

	public void run() {
		try {
			while(true){
            
				//Acepta la conexión del primer cliente que lo intenta
				Socket socket = server.accept();
				
				//al cerrar el socket, se espera hasta un máximo de 3 segundos a que el cliente termine de leer
				socket.setSoLinger(true, 3);
				
				//obteniendo la dirección ip del cliente
				String ipCliente = socket.getInetAddress().getHostAddress();
				
				//crea el objeto para escribir en el fichero log
		        ControladorLog log = new ControladorLog();
		        
		        Date date = new Date();
		        DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				
		        
				//registro log
		        log.println(hourdateFormat.format(date)+"\tconnection\t"+ ipCliente +" conexión entrante");
		        

				//crea el objeto ObjectOutputStream para enviar por el socket
		        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		        //crea el objeto ObjectInputStream para leer desde el socket
		        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		        
		        
		        //Write object to Socket
		        oos.writeObject("\nIngrese sus credenciales ");
		        //registro log
		        date = new Date();
		        log.println(hourdateFormat.format(date)+"\tresponse\tservidor envia respuesta a"+ ipCliente);

		        
		        /***Validación de usuario***/
		        boolean isLoginSuccess = false;
		        String user = "";
		        String pass = "";
		        String user2 = "";
		        String pass2 = "";
				try{
					String archivo = "usuarios";
					
					String line;
					for(int i=0; i<3 ; i++){
						FileReader fr = new FileReader(archivo);
						BufferedReader br = new BufferedReader(fr);
						user = (String) ois.readObject();
						System.out.println("User:" + user);

						pass = (String) ois.readObject();
						System.out.println("Pass: " + pass);

						while((line = br.readLine()) != null){
							user2 = line.split(" ")[0].toLowerCase();
							pass2 = line.split(" ")[1].toLowerCase();

							if(user.equals(user2) && pass.equals(pass2)){
								isLoginSuccess = true; //login exitoso
								oos.writeObject(isLoginSuccess);
								br.close();
								fr.close();
								break;
							}
						}

						br.close();
						fr.close();
						if(isLoginSuccess == true)
							break;
							
						//Error de usuario/contraseña
						oos.writeObject(isLoginSuccess);
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				if(isLoginSuccess == false){
					ois.close();
					oos.close();
					socket.close();
					continue;
				} 
		       

		        //Envia el objeto String al Cliente
		        oos.writeObject("Bienvenido " + user);
		        
		        
		        /***Ejecución comandos del usuario***/
		        String command;
		        do{
				    command = (String) ois.readObject(); //convierte ObjectInputStream a String
				    
				    //registro en log (comando)
				    date = new Date();
		        	log.println(hourdateFormat.format(date)+"\tcommand\t\t"+ ipCliente +" "+ command);
					
					String[] commandSplited = command.split("\\s");
					
		            switch(commandSplited[0]){
		            	case "ls":{
				            File dir = new File(".");
				            File [] ficheros = dir.listFiles();
				            oos.writeObject(ficheros);
				            break;
						}
						case "get":{
							
							String fichero = commandSplited[1];
							File f = new File(fichero);
							
							/*Si el archivo existe, entonces será enviado*/
							if (f.exists() && f.isFile() && f.canRead()) { 
								oos.writeObject("existe");
								boolean enviadoUltimo=false;
								// Se abre el fichero.
								FileInputStream fis = new FileInputStream(fichero);
								
								// Se instancia y rellena un mensaje de envio de fichero
								MensajeTomaFichero mensaje = new MensajeTomaFichero();
								mensaje.nombreFichero = fichero;
								
								// Se leen los primeros bytes del fichero en un campo del mensaje
								int leidos = fis.read(mensaje.contenidoFichero);
								
								// Bucle mientras se vayan leyendo datos del fichero
								while (leidos > -1)
								{
									
									// Se rellena el numero de bytes leidos
									mensaje.bytesValidos = leidos;
									
									// Si no se han leido el maximo de bytes, es porque el fichero
									// se ha acabado y este es el ultimo mensaje
									if (leidos < MensajeTomaFichero.LONGITUD_MAXIMA)
									{
										mensaje.ultimoMensaje = true;
										enviadoUltimo=true;
									}
									else
										mensaje.ultimoMensaje = false;
									
									// Se envia por el socket
									oos.writeObject(mensaje);
									
									// Si es el ultimo mensaje, salimos del bucle.
									if (mensaje.ultimoMensaje)
										break;
									
									// Se crea un nuevo mensaje
									mensaje = new MensajeTomaFichero();
									mensaje.nombreFichero = fichero;
									
									// y se leen sus bytes.
									leidos = fis.read(mensaje.contenidoFichero);
								}
								
								if (enviadoUltimo==false)
								{
									mensaje.ultimoMensaje=true;
									mensaje.bytesValidos=0;
									oos.writeObject(mensaje);
								}
							} else {
								oos.writeObject("cantRead");
							}
	
							
						    break;
						}
						
						case "put":{
							String fichero = commandSplited[1];				
							MensajeDameFichero mensaje = new MensajeDameFichero();
							mensaje.nombreFichero = fichero;

							MensajeTomaFichero mensajeRecibido;
							Object mensajeAux;
							
							mensajeAux = ois.readObject();
							if( ((String) mensajeAux).equals("cantRead") )
								break;
							
							// Se abre un fichero para empezar a copiar lo que se reciba.
							FileOutputStream fos = new FileOutputStream(mensaje.nombreFichero);
							do
							{
								// Se lee el mensaje en una variabla auxiliar
								mensajeAux = ois.readObject();

								// Si es del tipo esperado, se trata
								if (mensajeAux instanceof MensajeTomaFichero)
								{
								    mensajeRecibido = (MensajeTomaFichero) mensajeAux;
								    // Se escribe en el fichero
								    fos.write(mensajeRecibido.contenidoFichero, 0, mensajeRecibido.bytesValidos);
								} else
								{
								    // Si no es del tipo esperado, se marca error y se termina el bucle
								    //System.err.println("Mensaje no esperado " + mensajeAux.getClass().getName());
								    break;
								}
							} while (!mensajeRecibido.ultimoMensaje);
							
							// Se cierra socket y fichero
							fos.close();
						    break;
						}
						case "delete":{

							File f = new File(commandSplited[1]);
							
							/*Si el archivo existe, entonces será eliminado*/
							if (f.delete()) { 
					      		oos.writeObject(true);
				            }else{
				                oos.writeObject(false);
				            }
						    break;
						}
						case "exit":{
						    break;
						}
						default:{
							System.out.println("No se ha encontrado el comando: "+ commandSplited[0]);
						}

		            }
		            
		            //registro log
		            date = new Date();
        			log.println(hourdateFormat.format(date)+"\tresponse\tservidor envia respuesta a"+ ipCliente);
				    
				}while( !command.equalsIgnoreCase("exit") );
				
				
		        //close resources
		        ois.close();
		        oos.close();
		        socket.close();
		        //terminate the server if client sends exit request
        	}
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

}

