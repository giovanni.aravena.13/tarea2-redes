import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetAddress;



import java.util.concurrent.TimeUnit;

/**
 * This class implements java Socket server
 * @author pankaj
 *
 */
public class SocketServer {
    
    //static ServerSocket variable
    private static ServerSocket server;
    //puerto del servidor socket en el que escuchará
    private static int port = 9876;
    //numero de Threads a que responderan solicitudes de clientes
    private static final int NRO_THREADS = 2;

    
    
    public static void main(String args[]) throws IOException, ClassNotFoundException{

        //crear el objeto servidor de socket y Abre el socket
        server = new ServerSocket(port);
        
        System.out.println("Servidor Listo");
		System.out.println("Host Name: "+InetAddress.getLocalHost().getHostName());
        
		ControladorLog log = new ControladorLog();
		log.iniciarLog("DATETIME \t\t\tEVENT \t\tDESCRTIPTION\n");
        
        /*
		try {
			TimeUnit.SECONDS.sleep(15);//here is the error
		} catch (InterruptedException e) {
			e.printStackTrace(); // prints the exception stack trace to the console
		}
		*/
		     
        for(int i=0; i<NRO_THREADS; i++){
        	WorkerThread socketRequest = new WorkerThread(server);
        	socketRequest.start();
        }

        /*
        System.out.println("Para finalizar el servidor ingrese 'exit': ");
        String command = "On";
        while(!command.equalsIgnoreCase("exit")){
        	command = Leer.dato();
		}
		*/
		
        //close the ServerSocket object
        //server.close();
    }
    
}

//https://www.journaldev.com/741/java-socket-programming-server-client
