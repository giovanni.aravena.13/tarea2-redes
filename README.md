# Tarea 1 - Redes de Computadores


## Instrucciones de uso

 Al descomprimir, se crearán dos carpetas (Cliente y Servidor), en cada una de ellas abra una terminal y ejecute:

		1. make


 Luego, ejecute en cada terminal correspondiente:

		2. make runclient
		3. make runserver


 Para limpiar su escritorio:

		4. make clean


## Observaciones

	1. Los nombres de los archivos a intercambiar no deben contener espacios (Ejemplo: "Foto 1)
	2. Si desea añadir más usuarios, debe editar el archivo "usuarios.txt"


## Desarrolladores

* Giovanni Aravena Morales	201373598-3
* Fernando Llorens Carreño	201373528-2


## Versión del sistema de compilación de Java 
javac 11.0.3
openjdk 11.0.3 2019-04-16
OpenJDK Runtime Environment (build 11.0.3+7-Ubuntu-1ubuntu218.04.1)
OpenJDK 64-Bit Server VM (build 11.0.3+7-Ubuntu-1ubuntu218.04.1, mixed mode, sharing)

