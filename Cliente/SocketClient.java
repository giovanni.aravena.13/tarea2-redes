import java.util.concurrent.TimeUnit;
import java.io.*;
import java.util.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * This class implements java socket client
 * @author pankaj
 *
 */
public class SocketClient {

    public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException, InterruptedException{
        //get the localhost IP address, if server is running on some other IP, you need to use that
        InetAddress host = InetAddress.getLocalHost();
        Socket socket = null;
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        
        System.out.print("Ingrese la dirección IP del servidor: ");
        String ipServidor = Leer.dato();
        /*Verificar que la ip tiene el formato indicado*/
 
        //establish socket connection to server
        socket = new Socket(ipServidor, 9876);
        //socket = new Socket(host.getHostName(), 9876);
            
            
            //crea el objeto ObjectInputStream para leer desde el servidor
            ois = new ObjectInputStream(socket.getInputStream());
            //crea el objeto ObjectOutputStream para enviar al servidor
            oos = new ObjectOutputStream(socket.getOutputStream());

		
            //read the server response message
            String message = (String) ois.readObject();
            System.out.println(message);//Ingrese sus credenciales
            
            String user = "";
            String pass = "";
            boolean  isLoginSuccess = false;

            for(int i=0; i<3; i++){

                System.out.print("User:");
                user = Leer.dato();
                oos.writeObject(user);
            
                System.out.print("Pass:");
                pass = Leer.dato();
                oos.writeObject(pass);

                isLoginSuccess = (Boolean) ois.readObject();
                
                if(isLoginSuccess == true)
                    break;
                System.out.println("Error de autenticación");
            }

            if(isLoginSuccess == false){
                ois.close();
                oos.close();
                System.exit(0);
            }

            //read the server response message
            message = (String) ois.readObject();
            System.out.println("Server: " + message);
            
            //write
            String command;
            do{
		        System.out.print("\n"+user+"@"+host+":~/"+" $");
		        command = Leer.dato();
		        oos.writeObject(command);

				String[] commandSplited = command.split("\\s");
				
	            switch(commandSplited[0]){
	            	case "ls":{
			            File [] ficheros =  (File []) ois.readObject();
		                for(int i = 0; i < ficheros.length ; i++){
		                    System.out.println(ficheros[i].getName());
		                }
			            break;
					}
					case "get":{
						String fichero = commandSplited[1];
						MensajeDameFichero mensaje = new MensajeDameFichero();
						mensaje.nombreFichero = fichero;

						MensajeTomaFichero mensajeRecibido;
						Object mensajeAux;
						
						mensajeAux = ois.readObject();
						if( ((String) mensajeAux).equals("cantRead") )
							break;
						
						// Se abre un fichero para empezar a copiar lo que se reciba.
						FileOutputStream fos = new FileOutputStream(mensaje.nombreFichero);
						do
						{
						    // Se lee el mensaje en una variabla auxiliar
						    mensajeAux = ois.readObject();

						    // Si es del tipo esperado, se trata
						    if (mensajeAux instanceof MensajeTomaFichero)
						    {
						        mensajeRecibido = (MensajeTomaFichero) mensajeAux;
						        // Se escribe en el fichero
						        fos.write(mensajeRecibido.contenidoFichero, 0, mensajeRecibido.bytesValidos);
						    } else
						    {
						        // Si no es del tipo esperado, se marca error y se termina
						        // el bucle
						        System.err.println("Mensaje no esperado " + mensajeAux.getClass().getName());
						        break;
						    }
						} while (!mensajeRecibido.ultimoMensaje);
						
						// Se cierra socket y fichero
						fos.close();
            
            
					    break;
					}
					case "put":{
						String fichero = commandSplited[1];
						File f = new File(fichero);
						
						/*Si el archivo existe, entonces será enviado*/
						if (f.exists() && f.isFile() && f.canRead()) { 
							oos.writeObject("existe");
							boolean enviadoUltimo=false;
							// Se abre el fichero.
							FileInputStream fis = new FileInputStream(fichero);
							
							// Se instancia y rellena un mensaje de envio de fichero
							MensajeTomaFichero mensaje = new MensajeTomaFichero();
							mensaje.nombreFichero = fichero;
							
							// Se leen los primeros bytes del fichero en un campo del mensaje
							int leidos = fis.read(mensaje.contenidoFichero);
							
							// Bucle mientras se vayan leyendo datos del fichero
							while (leidos > -1)
							{
								
								// Se rellena el numero de bytes leidos
								mensaje.bytesValidos = leidos;
								
								// Si no se han leido el maximo de bytes, es porque el fichero
								// se ha acabado y este es el ultimo mensaje
								if (leidos < MensajeTomaFichero.LONGITUD_MAXIMA)
								{
									mensaje.ultimoMensaje = true;
									enviadoUltimo=true;
								}
								else
									mensaje.ultimoMensaje = false;
								
								// Se envia por el socket
								oos.writeObject(mensaje);
								
								// Si es el ultimo mensaje, salimos del bucle.
								if (mensaje.ultimoMensaje)
									break;
								
								// Se crea un nuevo mensaje
								mensaje = new MensajeTomaFichero();
								mensaje.nombreFichero = fichero;
								
								// y se leen sus bytes.
								leidos = fis.read(mensaje.contenidoFichero);
							}
							
							if (enviadoUltimo==false)
							{
								mensaje.ultimoMensaje=true;
								mensaje.bytesValidos=0;
								oos.writeObject(mensaje);
							}
						} else {
							oos.writeObject("cantRead");
							System.out.println("No se encuentra el archivo que ha especificado");
						}
						
					    break;
					}
					
					case "delete":{
						boolean isDeleted = false;

						isDeleted = (boolean) ois.readObject();
						if(isDeleted == true){
							System.out.println("Archivo Borrado");
						}
						else{
							System.out.println("No existe el archivo");
						}
						
					    break;
					}
					
					case "exit":{
					    break;
					}
					
					default:{
						System.out.println("No se ha encontrado el comando: "+command);
					}

	            }

		        
		        /***Hacer otra clase para realizar instrucciones del comando***/
				    	/*ls: listar contenido directorio actual*/
				    	/*get <nombre_archivo>: descargar desde el servidor (enviar archivo)*/
				    	/*put <nombre_archivo>: subir archivo desde el cliente (recibir archivo)*/
				    	/*delete <nombre_archivo>*/
			}while( !command.equalsIgnoreCase("exit") );
            
            //TimeUnit.SECONDS.sleep(1);
            
            //close resources
            ois.close();
            oos.close();
        
    }
}

